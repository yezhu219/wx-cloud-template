const BaseController = require('./BaseController')
const JobModle = require('../model/Job')
class Job extends BaseController {
  constructor(event,ctx) {
    super(event,ctx)
    this.model = new JobModle()
    
  }
   async detail() {
    this.validateParam('detail')
    this.validateData(this.param, this.model.getRule('detail'))
    let res = await this.model.detail(this.param._id)
    return this.success(res)
  }
  async add() {
    this.validateParam('add')
    this.validateData(this.param,this.model.getRule('add'))
    let res = await this.model.add(this.param)
    return this.success(res)
  }
  async del() {
    this.validateParam('del')
    this.validateData(this.param, this.model.getRule('del'))
    let res = await this.model.del(this.param._id)
    return this.success(res)
  }
  async update() {
    this.validateParam('update')
    this.validateData(this.param, this.model.getRule('update'))
    let res = await this.model.update(this.param)
    return this.success(res)
  }
  async page() {
    this.validateParam('page')
    this.validateData(this.param, this.model.getRule('page'))
    let res = await this.model.page(this.param)
    return this.success(res)
  }
}

module.exports = Job