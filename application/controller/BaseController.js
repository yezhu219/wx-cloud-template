const Validate = require('../validate')
const code = require('../config/errorCode')
const {show}  = require('../utils/utils')
class Controller {
  constructor(event,ctx) {
    this.event = event
    this.ctx = ctx
    this.router = event.router
    this.param = event.param
    this.validate = new Validate()

    this.paramRule = {
      pageSize:'require|number',
      pageNum:'require|number',
      keyword:'string',
      _id:'require|string'
    }
    this.scene= {
      page:'pageSize,pageNum',
      detail:'_id',
      update:'_id',
      del:'_id',
      find:''
    }
  }
  validateData(data,rule) {
    return this.validate.check(data,rule)
  }
  validateParam(scene='') {
    return this.validate.check(this.param,this.getRule(scene))
  }
  success(data) {
    return show(data,code.success,'ok')
  }
   /**
   * 根据场景获取对应规则
   * @param {*} scene 
   */
  getRule(scene) {
    if(!scene) return null
    let sceneRule = this.scene[scene]
    if(!sceneRule) return {}
    let arr = sceneRule.split(',')
    let rule = {}
    arr.forEach(item=> {
      rule[item] = this.paramRule[item]
    })
    return rule
  }
}

module.exports = Controller;