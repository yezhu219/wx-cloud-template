
const cloud = require('wx-server-sdk')
const config = require('../config/dbConfig')
const AppError = require('../exception/AppError')
const code = require('../config/errorCode')
cloud.init({
  env: config.CLOUD_ID
})
const db = cloud.database()
// console.log(cloud,'cloud')
class Model {
  constructor() {
    this.preFix = config.PRE_FIX
  }

  async detail(id) {
    let res = await db.collection(this.preFix+this.name).doc(id).get()
    console.log(res)
    return res.data
  }
  async page(data,$where={}) {
    let pageNum = data.pageNum
    let pageSize = data.pageSize
    if(pageSize>=100) {
      throw new AppError('分页每次最多查询100条数据',code.appError)
    }
    let countResult = await db.collection(this.preFix+this.name).count()
    let total = countResult.total
    let res = await db.collection(this.preFix+this.name).where($where).skip((pageNum-1)*pageSize).limit(pageSize).get()
    return {list:res.data || [],total}

  }

  async add(param) {
    let now = Date.now()
    param.create_time = param.update_time = now
    let res = await db.collection(this.preFix+this.name).add({ data: param })
    return {id:res._id}
  }
  async del(id) {
    let res = await db.collection(this.preFix+this.name).doc(id).remove()
    return res.stats.removed
  }
  async update(param) {
    let data = this.excludeId(param)
    data.update_time = Date.now()
    console.log(param,data)
    let res = await db.collection(this.preFix+this.name).doc(param._id).update({data})
    return res.stats
  }
  /**
   *  删除_id
   * @param {*} obj 
   */
  excludeId(obj) {
    if(!obj) return null
    let arr = Object.keys(obj)
    let data = {}
    arr.forEach(item=> {
      if(item!='_id') {
        data[item] = obj[item]
      }
    })
    return data
  }
  /**
   * 根据场景获取对应规则
   * @param {*} scene 
   */
  getRule(scene) {
    if(!scene) return null
    let sceneRule = this.scene[scene]
    if(!sceneRule) return {}
    let arr = sceneRule.split(',')
    let rule = {}
    arr.forEach(item=> {
      rule[item] = this.rule[item]
    })
    return rule
  }
}


module.exports = Model;