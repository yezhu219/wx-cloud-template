const BaseModel = require('./BaseModel')
class Job extends BaseModel {
  constructor() {
    super()
    this.rule = {
      '_id':'require|string',
      'classify_id':'require|number',
      'menber_limit':'string',
      'num':'require|number',
      'pay_type':'require|string',
      'phone':'require|string',
      'salary':'require|string',
      'station':'require|string',
      'welfare':'require|string',
      'work_type':'require|string',
      'work_position':'require|string',
      'work_time':'require|string',
      'status':'require|string',
    }
    this.scene={
      add:'classify_id,work_type,work_time,work_position',
      // add:'classify_id,menber_limit,num,pay_type,phone,salary,station,welfare,work_type,work_time,work_position',
      edit:'_id,classify_id,menber_limit,num,pay_type,phone,salary,station,welfare,work_type,work_time,work_position',
      detail:'_id',

    }
    this.name = 'job'
  }
}

module.exports = Job