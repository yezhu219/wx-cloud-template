module.exports = {
  // 成功
  success: 200,
  // 路由错误
  routerError: 1001, 
  // 系统错误
  appError: 2000,
  // 数据校验错误
  dataError:3000,
  // 未知错误
  unknow:5000

}