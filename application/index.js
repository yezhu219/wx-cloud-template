 const {show,firstUpper} = require('./utils/utils')
 const code = require('./config/errorCode')
 const AppError = require('./exception/AppError')
 const RouterError = require('./exception/RouterError')
 const path = require('path')
 const fs = require('fs')
const DataError = require('./exception/DataError')

 class App {
   constructor(event,ctx) {
     this.event = event
     this.ctx = ctx
    //  默认状态码
     this.code = 200
    //  默认提示  
     this.msg = 'ok'
    //  默认返回数据
     this.data = null
     this.isError = false
   }
   async init() {
     try {
      this.validateRouter()
      await this.initController()
      // this.errorHandle()
      return show(this.data,this.code,this.msg)
     } catch (e) {
       console.log(e,'e')
       if(e.name!='AppError'||e.name!='DataError'||e.name!='RouterError') {
         return show(null,code.unknow,e.message)
       }else {
         return show(null,e.code,e.message)
      }
     }
     
   }
   // 加载控制器，执行对应方法，返回结果
   async initController() {
      await this.validateController()
   }
  //  验证路由规则
   validateRouter() {
      let router = this.event.router
      if(!router) {
        throw new RouterError('路由错误',code.routerError)
      }else if(typeof router !='string') {
        throw new RouterError('路由必须是字符串',code.routerError)
      }else if(!this.checkRoute(router)) {
        throw new RouterError('路由格式不正确',code.routerError)
      }

    }
    // 验证控制器
    async validateController() {
    let controllerName = this.event.router.split('/')[0]
    let fuctionName = this.event.router.split('/')[1]
    controllerName = firstUpper(controllerName)
    let bol = fs.existsSync(path.join(__dirname,`./controller/${controllerName}.js`))
    if(bol) {
      let ControllerClass = require(path.join(__dirname,`./controller/${controllerName}.js`))
      const controller = new ControllerClass(this.event,this.ctx)
      if(this.hasMethod(controller,fuctionName)) {
        let res =  await controller[fuctionName]();
        console.log(res,'ss')
        if(res) { 
          this.setResponeData(res.data,res.code,res.msg)
          return show(this.data,this.code,this.msg)
        }
      }else {
        throw new RouterError('找不到对应控制器方法',code.routerError)
      }
    }else {
      throw new RouterError('找不到对应控制器',code.routerError,)
    }
   } 
   errorHandle() {
     process.on('unhandledRejection',(e)=> {
      console.log(e.message,e.name,e.code)
      this.isError = true
      this.setResponeData(null,e.code,e.message)
      // return show(this.data,this.code,this.msg)
       throw new DataError(e.message,e.code)
     })
   }
   /**
    * 判断控制器是否存在方法
    * @param {*} obj 
    * @param {*} method 
    */
   hasMethod(obj,method) {
    if(obj[method]&&typeof obj[method]=='function') {
      return true
    }else {
      return false
    }
   }
   /**
    * 设置返回数据
    * @param {*} data 
    * @param {*} code 
    * @param {*} msg 
    */
   setResponeData(data,code,msg) {
    this.data = data
    this.code = code
    this.msg = msg
   }
  //  校验router格式  controller/function
   checkRoute(str) {
    return /^[a-zA-Z]+\/[a-zA-Z]+$/.test(str)
  }
 }

 module.exports = App