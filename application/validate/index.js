
const DataError = require('../exception/DataError')
const code = require('../config/errorCode')


class Validate {
  isString(data) {
    return typeof data=='string'
  }
  isNumber(data) {
    return typeof data == 'number'
  }
  isArray(data) {
    return Array.isArray(data)
  }
  isObject(data) {
    return data == Object(data)
  }
  isRequire(data) {
    return !!data
  }
  isBoolean(data) {
    return typeof data === 'boolean';
  }
  isDefine(data) {
    return typeof data===undefined
  }
  isEmpty(data) {
    if (this.isArray(data)) {
      return data.length==0
    } else if (this.isObject(data)) {
      return JSON.stringify(data) == '{}'
    } else if (this.isString(data)) {
      return data === ''
    } else if(data===undefined) {
      return true
    }
  }

  isPhone(data) {
    return /(^1[3|5|8][0-9]{9}$)/.test(value)
  }
  isEmail(data) {
    let reg = /^[A-Za-z0-9+]+[A-Za-z0-9\.\_\-+]*@([A-Za-z0-9\-]+\.)+[A-Za-z0-9]+$/;
    return reg.test(data)
  }
  isInArray(data,arrSting) {
    let arr = arrSting.split(',')
    return arr.some(item=>item==data)
  }



  check(data,rules) {
    if (!rules || this.isEmpty(rules)) return true
    let ruleKeys = Object.keys(rules)
    ruleKeys.forEach(key => {
      let ruleArr = rules[key].split('|')
      this.checkData(data,ruleArr,key)
    })
  }
  checkData(data, ruleArr, key) {
    let errCode = code.dataError
    ruleArr.forEach(rule => {
      let ruleName = rule
      let ruleContent = ''
      if (/=/.test(rule)) {
        let arr = rule.split('=')
        ruleName = arr[0]
        ruleContent = arr[1]
      }
      switch (ruleName) {
        case 'require':
          if (this.isEmpty(data) || this.isDefine(data[key])) {
            throw new DataError(key+'不能为空',errCode)
          }
          break;
        case 'string':
        case 'str':
          if (!this.isString(data[key])) {
            throw new DataError(key + '必须是字符串', errCode)
          }
          break;
        case 'number':
        case 'num':
          if (!this.isNumber(data[key])) {
            throw new DataError(key + '必须是数字', errCode)
          }
          break;
        case 'boolean':
        case 'bool':
          if (!this.isBoolean(data[key])) {
            throw new DataError(key + '必须是布尔值', errCode)
          }
          break;
        case 'email':
          if (!this.isEmail(data[key])) {
            throw new DataError(key + '不符合邮箱格式', errCode)
          }
          break;
        case 'phone':
        case 'tel':
          if (!this.isPhone(data[key])) {
            throw new DataError(key + '不符合手机格式', errCode)
          }
          break;
        case 'in':
          let contentArr = ruleContent.split[',']
          if (!contentArr.includes(data[key])) {
            throw new DataError(key + '必须不在'+ruleContent+'中', errCode)
          }
      }
    })
  }

}

module.exports = Validate