const code = require('../config/errorCode')

class DataError extends Error {
  constructor(msg,code=code.dataError) {
    super(msg)
    this.code = code
    this.name = 'DataError'
  }
}

module.exports = DataError