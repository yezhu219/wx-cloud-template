const errorCode = require('../config/errorCode')

class RouterError extends Error {
  constructor(msg,code=errorCode.routerError) {
    super(msg)
    this.code = code
    this.name = 'routerError'
  }
}

module.exports = RouterError