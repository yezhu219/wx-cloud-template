const code = require('../config/errorCode')

class AppError extends Error {
  constructor(msg,code=code.appError) {
    super(msg)
    this.code = code
    this.name = 'AppError'
  }
}

module.exports = AppError