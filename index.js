// 云函数入口文件
// const cloud = require('wx-server-sdk')
// cloud.init({
//     env: cloud.DYNAMIC_CURRENT_ENV
//   })
// const db = cloud.database()
const App = require("./application/index")
// 云函数入口函数
exports.main = async (event, context) => {
  const app = new App(event,context)
  return app.init()
}