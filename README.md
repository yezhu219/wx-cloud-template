# wx-cloud-template

#### 介绍
基于小程序云开发MVC封装，项目使用node进行开发，参考TP5目录结构及逻辑进行封装。

小程序云开发未提供数据校验，因此想做一套数据校验入库的封装


#### 软件架构

│  .gitignore
│  index.js    入口文件
│  LICENSE
│  package.json
│  README.md
│
└─application   应用文件夹
    │  index.js 应用入口
    │
    ├─config    配置
    │      dbConfig.js  数据库配置
    │      errorCode.js 错误码配置
    │
    ├─controller    控制器
    │      BaseController.js  基础控制器
    │      Job.js
    │
    ├─exception  异常类
    │      AppError.js   
    │      DataError.js
    │      RouterError.js
    │ 
    ├─model   model层
    │      BaseModel.js  
    │      Job.js
    │
    ├─service  服务成
    ├─utils 工具函数
    │      checkData.js
    │      utils.js
    │
    └─validate  验证器
            index.js


#### 项目进度


 - [x]    contoller 逻辑
 - [ ]    model 逻辑
 - [ ]    validate逻辑


#### 使用说明

##### 配置项

config/dbConfig   数据库相关配置

config/errorCode  错误码配置

##### 路由使用
云函数调用函数名为 cloud,router为必填参数，对应规则为控制器/控制器方法，param为请求参数
```js
 wx.clould.callFunction({
     name:'cloud',
     data:{
        "router": "job/update",
        "param": {
            "_id": "28ee4e3e60587b210bfbd4f66b2c8c4d",
            "classify_id": 12,
            "work_type": "22222",
            "work_position": "武汉2",
            "work_time": "8:00-10:00"
        }
    }
 })
```

##### 验证器

参照TP5场景逻辑，封装了一套验证逻辑，分为路由参数验证及数据入库校验

-  参数验证
controller/BaseController 场景scene中默认提供了分页、详情、新增、更新及删除场景， paramRule中定义了常用场景的检验参数及规则

- 数据入库校验

model 中定义了该模型的验证规则及场景


##### controller 使用

controller 需继承BaseController，提供了校验参数方法validateParam，及数据入库校验 validateData

````js
const BaseController = require('./BaseController')
const JobModle = require('../model/Job')
class Job extends BaseController {
  constructor(event,ctx) {
    super(event,ctx)
    this.model = new JobModle()
    
  }
   async detail() {
    this.validateParam('detail')
    this.validateData(this.param, this.model.getRule('detail'))
    let res = await this.model.detail(this.param._id)
    return this.success(res)
  }
  async add() {
    this.validateParam('add')
    this.validateData(this.param,this.model.getRule('add'))
    let res = await this.model.add(this.param)
    return this.success(res)
  }
  async del() {
    this.validateParam('del')
    this.validateData(this.param, this.model.getRule('del'))
    let res = await this.model.del(this.param._id)
    return this.success(res)
  }
  async update() {
    this.validateParam('update')
    this.validateData(this.param, this.model.getRule('update'))
    let res = await this.model.update(this.param)
    return this.success(res)
  }
  async page() {
    this.validateParam('page')
    this.validateData(this.param, this.model.getRule('page'))
    let res = await this.model.page(this.param)
    return this.success(res)
  }
}

module.exports = Job
```


#####  异常处理

exception 定义自定义异常，自定义异常需继承Error类，指定类名及code

##### 数据校验

validate中定义校验规则，可自行扩展





